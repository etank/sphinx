# README

This site is generated using the Sphinx Python Document Generator. It also makes use of the Read The Docs theme.

## Requirements

On a Linux based system (Fedora 26) install the following packages:

* git
* python2-sphinx
* python2-recommonmark

On a RHEL 7.3 system the git package is still required however the sphinx pieces need to be installed via pip:

* sphinx==1.6.3
* sphinx-rtd-theme==0.2.4

## Cloning the site

Once you have the requirements installed the git command to clone the project locally.

```shell
git clone git@git.badger.net:dream/portal.git
```

## Building the site locally

Using the built in Makefile run the following command to build the html pages.

```shell
make html
```

The html pages should be accessible in the `<path_to_project>/build/html` folder.

## Contributing to the docs

Create a fork of the project in gitlab by visiting the http://git.badger.net/dream/portal URL and click on the **Fork** button. This will allow you to have your own copy of the project in your own space in gitlab.

Make your changes to your fork and once you have tested them to ensure that the site looks correct create a merge request. Click the **Merge Requests** link in your fork then the **New merge request** button. On the source branch side of the screen select the name of the branch that your updates are in. This will most likely be the **master** branch. Then click **Compare branches and continue**. Verify that there are no merge conflicts and then use the **Submit merge request** button to create the merge request.
