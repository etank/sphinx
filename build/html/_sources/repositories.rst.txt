:gitlab_url: http://git.badger.net/dream/portal

Repositories
============

+--------------------------+-------------------------------------------+
| Repository Name          | URL                                       |
+==========================+===========================================+
| ISO Images               | http://images.com                         |
+--------------------------+-------------------------------------------+
| Jcenter                  | http://jcenter.bintray.com                |
+--------------------------+-------------------------------------------+
| Ruby Gems                | http://rubygems.org                       |
+--------------------------+-------------------------------------------+
| PyPI                     | http://pypi.python.org                    |
+--------------------------+-------------------------------------------+
| Google Download          | http://dl.google.com                      |
+--------------------------+-------------------------------------------+
| Postgresql               | http://yum.postgresql.org                 |
+--------------------------+-------------------------------------------+
| Docker Registry          | https://index.docker.io                   |
+--------------------------+-------------------------------------------+
| CentOS Cloud Images      | http://cloud.centos.org                   |
+--------------------------+-------------------------------------------+
| Ubuntu Cloud Images      | http://cloud-images.ubuntu.com/           |
+--------------------------+-------------------------------------------+
| Cygwin                   | http://cygwin.com/                        |
+--------------------------+-------------------------------------------+
| beepbeep.sourceforge.net | http://beebeep.sourceforge.net            |
+--------------------------+-------------------------------------------+

======
Ubuntu
======

+--------------------+-----------------------------------------------------+
| Version            | URL                                                 |
+====================+=====================================================+
| Trusty LTS (14.04) | http://us.archive.ubuntu.com/ubuntu/dists/trusty/   |
+--------------------+-----------------------------------------------------+
| Xenial LTS (16.04) | http://us.archive.ubuntu.com/ubuntu/dists/xenial/   |
+--------------------+-----------------------------------------------------+
| Artful (17.10)     | http://us.archive.ubuntu.com/ubuntu/dists/artful/   |
+--------------------+-----------------------------------------------------+

======
CentOS
======

+---------+------------------------------------+
| Version | URL                                |
+=========+====================================+
| 6       | http://mirror.centos.org/centos/6/ |
+---------+------------------------------------+
| 7       | http://mirror.centos.org/centos/7/ |
+---------+------------------------------------+

======
Fedora
======

+---------+-----------------------------------------------------+
| Version | URL                                                 |
+=========+=====================================================+
| 26      | http://mirrors.fedoraproject.org/linux/releases/26/ |
+---------+-----------------------------------------------------+

.. | 27      | http://mirrors.fedoraproject.org/linux/releases/27/ |
.. +---------+-----------------------------------------------------+

====
EPEL
====

+---------+-----------------------------------------------------+
| Version | URL                                                 |
+=========+=====================================================+
| EL 7    | http://download.fedoraproject.org/pub/epel/7/x86_64 |
+---------+-----------------------------------------------------+

======
Debian
======

+------------+-------------------------------------------+
| Version    | URL                                       |
+============+===========================================+
| Wheezy (7) | http://us.debian.org/debian/dists/wheezy/ |
+------------+-------------------------------------------+
| Jessie (8) | http://us.debian.org/debian/dists/jessie/ |
+------------+-------------------------------------------+

==========
Linux Mint
==========

+--------------+----------------------------------------------+
| Version      | URL                                          |
+==============+==============================================+
| Rebecca (16) | http://packages.linuxmint.com/dists/rebecca/ |
+--------------+----------------------------------------------+
| Rosa (17)    | http://packages.linuxmint.com/dists/rosa/    |
+--------------+----------------------------------------------+
| Sarah (18)   | http://packages.linuxmint.com/dists/sarah/   |
+--------------+----------------------------------------------+

==========
Kali Linux
==========

+----------------+-----------------------------------------------+
| Version        | URL                                           |
+================+===============================================+
| Sana           | http://http.kali.org/kali/dists/sana/         |
+----------------+-----------------------------------------------+
| Rolling Release| http://http.kali.org/kali/dists/kali-rolling/ |
+----------------+-----------------------------------------------+

========
Raspbian
========

+------------+----------------------------------------------------+
| Version    | URL                                                |
+============+====================================================+
| Wheezy (7) | http://archive.raspbian.org/raspbian/dists/wheezy/ |
+------------+----------------------------------------------------+

