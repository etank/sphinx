:gitlab_url: http://git.badger.net/dream/portal

Welcome to the 90 COS Portal
============================

* :ref:`openstack`
* :ref:`applications`
* :ref:`repositories`
* :ref:`documentation`

.. _openstack:

.. toctree::
   :maxdepth: 2
   :caption: OpenStack Links
   
   openstack

.. _applications:

.. toctree::
   :maxdepth: 2
   :caption: Application Links

   applications

.. _repositories:

.. toctree::
   :maxdepth: 2
   :caption: Repository Links

   repositories
   
.. _documentation:

.. toctree::
   :maxdepth: 2
   :caption: Documetation Links

   documentation

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
