:gitlab_url: http://git.badger.net/dream/portal

OpenStack Links
===============

New
---

The new OpenStack build is OSP Version 10. New projects should be added here.

* **Red Hat OpenStack 10**: `osp10.badger.net <http://osp10.badger.net>`_

Legacy
------

The legacy OpenStack server is below. 

.. warning:: No new projects should be added here and we should be migrating them to the new build.

* **Legacy**: `openstack.badger.net <http://openstack.badger.net>`_
